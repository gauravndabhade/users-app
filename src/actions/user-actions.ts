import {Action} from '@ngrx/store';
import {UsersActionTypes} from '../shared/enum/user-action-type.enum';

export class ActionParent implements Action {
    type: any;
    payload: any;
}

export class UserAdd implements ActionParent {
    type: UsersActionTypes.Add;
    constructor(public payload: any) {}
}
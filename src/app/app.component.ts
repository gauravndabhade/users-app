import { Component, OnInit } from '@angular/core';
import {User} from '../models/user.model';
import {Observable} from 'rxjs';
import {UserAdd} from '../actions/user-actions'
import {select, Store} from '@ngrx/store';
import { NgForm } from '@angular/forms';    


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit{
  
  users: User[];

  constructor(private store: Store<{users : User[]}>) {
    store.pipe(select('users')).subscribe(values => {
      console.log(values);
      this.users = values;
    })
  }

  addUser(e) {
    console.log('value', e);
    // this.store.dispatch(new UserAdd(user));
  }

  ngOnInit() {
  }
}

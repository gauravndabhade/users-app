import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule} from '@ngrx/store';
import { AppComponent } from './app.component';
import { UserReducer } from 'src/reducer/user.reducer';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    StoreModule.forRoot({users : UserReducer})
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

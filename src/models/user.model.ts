export interface User {
    name: String;
    email: String;
    contact: String;
}
import {UsersActionTypes} from '../shared/enum/user-action-type.enum';
import {ActionParent } from '../actions/user-actions';
import {User} from '../models/user.model';

export const initialState: User[] = [
    {
        name: 'gaurav',
        email: 'gaurav@gmail.com',
        contact: '9876543210'
    },
    {
        name: 'pavan',
        email: 'pavan@gmail.com',
        contact: '9876543210'
    },
    {
        name: 'bhushan',
        email: 'bhushan@gmail.com',
        contact: '9876543210'
    }
];


export function UserReducer (state = initialState, action: ActionParent) {
    switch(action.type) {
        default: return state
    }
}